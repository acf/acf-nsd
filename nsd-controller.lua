local mymodule = {}
validator = require("acf.validator")

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.listfiles(self)
	return self.model.list_files(self)
end

function mymodule.editfile(self)
	return self.handle_form(self, self.model.get_file, self.model.update_file, self.clientdata, "Save", "Edit File", "File Saved")
end

function mymodule.createfile(self)
	return self.handle_form(self, self.model.get_new_file, self.model.create_file, self.clientdata, "Create", "Create New NSD File", "Freeswitch File Created")
end

function mymodule.deletefile(self)
	return self.handle_form(self, self.model.get_delete_file, self.model.delete_file, self.clientdata, "Delete", "Delete NSD File", "Freeswitch File Deleted")
end

function mymodule.listzonefiles(self)
	return self.model.getzonefilelist(self, self.clientdata, self.sessiondata.userinfo.userid)
end

function mymodule.editzonefile(self)
	config = self.handle_form(self, function()
			return self.model.get_zonefile(self, self.clientdata, self.sessiondata.userinfo.userid)
		end, function(self, value)
			return self.model.set_zonefile(self, value, self.sessiondata.userinfo.userid)
		end, self.clientdata, "Save", "Edit Zone File", "Zone File Saved")

	if self.clientdata.linenumber and validator.is_integer(self.clientdata.linenumber) then
		config.value.filecontent.linenumber = self.clientdata.linenumber
	end
	return config
end

function mymodule.listpermissions(self)
	return self.model:getpermissionslist()
end

function mymodule.edituserpermissions(self)
	return self.handle_form(self, self.model.getuserpermissions, self.model.setuserpermissions, self.clientdata, "Save", "Edit User Permissions", "User permissions set")
end

function mymodule.editrolepermissions(self)
	return self.handle_form(self, self.model.getrolepermissions, self.model.setrolepermissions, self.clientdata, "Save", "Edit Role Permissions", "Role permissions set")
end

return mymodule
